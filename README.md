# Flow1 is manufacturing test for HLHAT board also known as Pilot
This version now failing but has worked - check

## starting node red manually - this needs automating

pi@raspberrypi:~ $ node-red


## tests
ttyAMA0 - HL module - physical serial port  
PSU voltage by module AT command - TX 1800 state 0  
SIM presence 
power
ttyACM0 

## Needs adding 
  
Time out for overall test process  
power test needs to be module specific - inprogress

## added
Voltmeter for power test 4G only - [hardware and software]  - needs documenting


## Git "checkin"
git add .  

git commit -m "comment"  

git push -u origin master  


## Git "checkout"
git clone https://gitlab.com/johnofleek/hlhatManuTest  


## capture node-red start
```
6 Sep 08:58:27 - [info] Node-RED version: v0.19.1
6 Sep 08:58:27 - [info] Node.js  version: v8.11.4
6 Sep 08:58:27 - [info] Linux 4.14.52-v7+ arm LE
6 Sep 08:58:31 - [info] Loading palette nodes
6 Sep 08:58:42 - [info] Settings file  : /home/pi/.node-red/settings.js
6 Sep 08:58:42 - [info] Context store  : 'default' [module=memory]
6 Sep 08:58:42 - [info] User directory : /home/pi/.node-red
6 Sep 08:58:42 - [warn] Projects disabled : editorTheme.projects.enabled=false
6 Sep 08:58:42 - [info] Flows file     : /home/pi/.node-red/flows_raspberrypi.json
6 Sep 08:58:42 - [info] Server now running at http://127.0.0.1:1880/
```

## additional modules
1. For ADS1015 ADC (should have used ADS1115)
npm i node-red-contrib-ads1x15

2. A stoppable event generator
https://flows.nodered.org/node/node-red-contrib-looptimer



