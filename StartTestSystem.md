
# Connect up
1. The raspberry pi must be connected to the network via the Ethernet port - it will work without this but it will cause issues later on

# Power up
1. Apply power to the Raspberry PI micro usb
2. Apply power to the 5V wire
3. Apply power to the 9V wire

# Test tool start up 
1. Open a Shell <CTRL><ALT><T>
2. Type node-red
3. The following should appear in the shell

```
pi@raspberrypi:~ $ node-red
13 Sep 09:25:16 - [info] 

Welcome to Node-RED
===================

13 Sep 09:25:16 - [info] Node-RED version: v0.19.1
13 Sep 09:25:16 - [info] Node.js  version: v8.11.4
13 Sep 09:25:16 - [info] Linux 4.14.52-v7+ arm LE
13 Sep 09:25:18 - [info] Loading palette nodes
13 Sep 09:25:31 - [info] Settings file  : /home/pi/.node-red/settings.js
13 Sep 09:25:31 - [info] Context store  : 'default' [module=memory]
13 Sep 09:25:31 - [info] User directory : /home/pi/.node-red
13 Sep 09:25:31 - [warn] Projects disabled : editorTheme.projects.enabled=false
13 Sep 09:25:31 - [info] Flows file     : /home/pi/.node-red/flows_raspberrypi.json
13 Sep 09:25:31 - [info] Server now running at http://127.0.0.1:1880/
13 Sep 09:25:31 - [warn] 

---------------------------------------------------------------------
Your flow credentials file is encrypted using a system-generated key.

If the system-generated key is lost for any reason, your credentials
file will not be recoverable, you will have to delete it and re-enter
your credentials.

You should set your own key using the 'credentialSecret' option in
your settings file. Node-RED will then re-encrypt your credentials
file using your chosen key the next time you deploy a change.
---------------------------------------------------------------------

13 Sep 09:25:31 - [info] Starting flows
13 Sep 09:25:32 - [info] Started flows
13 Sep 09:25:32 - [error] serial port /dev/ttyACM0 error: Error: Error: No such file or directory, cannot open /dev/ttyACM0
13 Sep 09:25:32 - [info] serial port /dev/ttyAMA0 opened at 115200 baud 8N1
13 Sep 09:25:47 - [info] serial port /dev/ttyACM0 opened at 57600 baud 8N1

```

# Start test
1. Open web browser (blue circle at top left (chromium))
2. Test url is http://localhost:1880/web
3. Opening the url starts the test 

4. To repeat the test or test another unit hit the web browser refresh button
