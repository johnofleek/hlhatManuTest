# Connect up

    The raspberry pi must be connected to the network via the Ethernet port - it will work without this but it will cause issues later on
*    Keyboard - RPi
*    Mouse - RPi
*    HDMI to RPI
*    USB A to micro USB UUT cable


# Power
1. Apply 5V power to RPi 4mm connectors
2. Apply power to the 5V UUT 4mm connectors
3. Apply power to the 9V 4mm connectors

# Test software start up after RPi has booted
1. Open a Shell <CTRL><ALT><T> (or Black box TOP RHS >_)
2. Type node-red
3. The following should appear in the shell


```
pi@raspberrypi:~ $ node-red
13 Sep 09:25:16 - [info]

Welcome to Node-RED
===================

...

```

Leave shell open

4. Open web browser (shortcut or 127.0.0.1:1880/web)



# Test

 1. Micro USB to RPI
 2. SIM (test SIM is inserted in gold board)
 3. GNSS RF (LHS)
 4. Cellular RF (RHS)
 5. Insert UUT into fixture (be careful the location pins are weak) and close the clamp
 6. Browser refresh test should run
 7. Remove UUT